<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Attribute;
use Auth;

class HomeController extends Controller
{
    public function index(Attribute $attribute) {
		if ($user = Auth::user()) {
			$attributes = $attribute->where("user_id", "=", Auth::user()->id)->get();
			return view('main', compact('attributes'));
		}
		else
			return view('main');
	}
}
