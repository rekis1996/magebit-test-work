$(document).ready(function () {
    $('.name').hide()
	
	$( "#name" ).focus(function() {
		$('.name-label').addClass('name-label-toggle')
		$('.name-label').removeClass('name-label')
	});
	$( "#name" ).blur(function() {
		$('.name-label-toggle').addClass('name-label')
		$('.name-label-toggle').removeClass('name-label-toggle')
	});
	
	$( "#email" ).focus(function() {
		$('.email-label').addClass('email-label-toggle')
		$('.email-label').removeClass('email-label')
	});
	$( "#email" ).blur(function() {
		$('.email-label-toggle').addClass('email-label')
		$('.email-label-toggle').removeClass('email-label-toggle')
	});
	
	$( "#password" ).focus(function() {
		$('.password-label').addClass('password-label-toggle')
		$('.password-label').removeClass('password-label')
	});
	$( "#password" ).blur(function() {
		$('.password-label-toggle').addClass('password-label')
		$('.password-label-toggle').removeClass('password-label-toggle')
	});
	
	$('#error-popup').delay(3000).fadeOut(300);
	$('#flash-message').delay(3000).fadeOut(300);
	
    $('button.sign-up-switch').on('click', function () {

        if (animationFinished == 1) {
				$('h3.with-logo').fadeOut(500)
				$('a.forget').fadeOut(500)
				$('#panel_line').fadeOut(500)
				$('form').fadeOut(500, function () {
                $('form').fadeIn(500)
				$('#panel_line').fadeIn(500)
                $('.name').fadeIn(500)
                $('h3.with-logo').fadeIn(500)
                $('button.in-form').html('Sign Up')
                $('h3.with-logo').html('Sign Up')
				$("#name").prop('required',true)
            })

            $('form').attr('action', '/register')
        }

        var currentLeftPos = 0;
        var element = document.getElementById("panel-element");
        currentLeftPos = (element.offsetLeft - element.scrollLeft + element.clientLeft);

        if (currentLeftPos >= initialLeftPos && animationFinished == 1) {
            animationFinished = 0
            $('.form-panel').css('position', 'relative').animate({
                    left: '-=420px'
                },
                1000,
                function () {
                    animationFinished = 1
                })
        }
    })

    $('button.log-in-switch').on('click', function () {

        if (animationFinished == 1) {
				$('h3.with-logo').fadeOut(500)
				$('.name').fadeOut(500)
				$('#panel_line').fadeOut(500)
				$('form').fadeOut(500, function () {
                $('form').fadeIn(500)
				$('#panel_line').fadeIn(500)
                $('h3.with-logo').fadeIn(500)
                $('button.in-form').html('Login')
                $('h3.with-logo').html('Login')
                $('a.forget').fadeIn(500)
				$("#name").prop('required',false)
            })

            $('form').attr('action', '/login')
        }

        var currentLeftPos = 0;
        var element = document.getElementById("panel-element");
        currentLeftPos = (element.offsetLeft - element.scrollLeft + element.clientLeft);

        if (currentLeftPos <= initialLeftPos - 420 && animationFinished == 1) {
            animationFinished = 0
            $('.form-panel').css('position', 'relative').animate({
                left: '+=420px'
            }, 1000, function () {
                animationFinished = 1

            })
        }
    })

    var initialLeftPos = 0;
    var animationFinished = 1;
    $(
        function setupPos() {
            var element = document.getElementById("panel-element");
            initialLeftPos = (element.offsetLeft - element.scrollLeft + element.clientLeft);
        }
    )

})