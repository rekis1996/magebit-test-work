@if($user = Auth::user())
	<div id="login-message" class="alert alert-info top-logged" role="alert">
		You are logged in, {{ $user->name }}! Click <a href="/logout">here</a> to log out.
	</div>
@endif