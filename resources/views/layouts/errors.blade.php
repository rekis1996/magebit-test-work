@if (count($errors))
    <div id="error-popup" class="alert alert-danger top-alert" role="alert">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif