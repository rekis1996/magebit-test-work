@if($flash = session('message'))
	<div id="flash-message" class="alert alert-success top-messages" role="alert">
		{{ $flash }}
	</div>
@endif