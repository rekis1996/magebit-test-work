<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Magebit test work</title>

        <!-- Styles -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet" type="text/css">
		<link href="css/media-queries.css" rel="stylesheet" type="text/css">

    </head>
    <body>
		<div class="container-fluid">
			@if($user = Auth::user())
			<div class="login-panel" style="display: block;">
				<div class="col-sm-6">
					<div class="panel-left">
						<h3>Hello, {{ $user->name }}!</h3>
						<hr>
						<p>Your email: {{ $user->email }}</p>
						<p>Registration date: {{ $user->created_at }}</p>
						<p>Profile attributes:</p>
						@if($user->attributes()->count() > 0)
							<ul>
							@foreach($attributes as $attribute)
								<li>{{ $attribute->name }}: {{ $attribute->value }}</li>
							@endforeach
							</ul>
						@endif
						<a href="/logout" class="btn btn-primary">Log out</a>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel-right">
						<h3>Add attribute!</h3>
						<hr>
						<div class="attribute_add">
							<form action="/attributes" method="POST">
								{{ csrf_field() }}
								<div class="form-group">
									<label for="attribute_name">Name<sup>*</sup></label>
									<input type="text" class="form-control" name="attribute_name" id="attribute_name">
								</div>
								<div class="form-group">
									<label for="value">Value<sup>*</sup></label>
									<input type="value" class="form-control" name="value" id="value" required>
								</div>
								<button type="submit" class="btn btn-warning log-in in-form">Add</button>
							</form>
						</div>
					</div>
					<!-- end panel-left -->
				</div>
			</div>
			@else
			<div class="login-panel" style="display: block;">
				<div class="col-sm-6">
					<div class="panel-left">
						<h3>Don't have an account?</h3>
						<hr>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						<button class="btn btn-primary sign-up-switch">Sign up</button>
					</div>
					<!-- end panel-left -->
				</div>
				<!-- end col-sm-6 -->
				<div class="col-sm-6">
					<!-- embedded when jQuery animate() triggered -->
					<div class="panel-right" style="display:block;">
						<h3>Already have an account?</h3>
						<hr>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						<button class="btn btn-primary log-in-switch">Login</button>
					</div>
					<!-- end panel-left -->
					<!-- end embedded -->
				</div>
				
				@yield('content')
				
			</div>
			<!-- end login-panel -->
			@endif


		@include('layouts.errors')
		@include('layouts.messages')
		@include('layouts.logged')
		</div>
		<footer>All rights reserved "MageBit" 2016.</footer>
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<script src="js/effects.js"></script>
    </body>
</html>