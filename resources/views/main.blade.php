@extends('layouts.master')

@section('content')
<!-- end col-sm-6 -->
<div class="form-panel log-in" id="panel-element">
	<h3 class="with-logo">Login</h3>
	<hr id="panel_line">
	<form action="/login" method="POST">
		{{ csrf_field() }}
		<div class="form-group name" style="display:none;">
			<label for="name" class="name-label">Name<sup>*</sup></label>
			<input type="text" class="form-control" name="name" id="name">
		</div>
		<div class="form-group">
			<label for="email" class="email-label">Email<sup>*</sup></label>
			<input type="email" class="form-control" name="email" id="email" required>
		</div>
		<div class="form-group">
			<label for="password" class="password-label">Password<sup>*</sup>
			</label>
			<input type="password" class="form-control" name="password" id="password" required>
		</div>
		<button type="submit" class="btn btn-warning log-in in-form">Login</button>
		<a href="/forgot" class="forget">Forgot?</a>
	</form>
</div>
<!-- end form-panel -->
@endsection